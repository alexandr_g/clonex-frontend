'use strict';

/**
 * @ngdoc function
 * @name projectxApp.controller:StylisCtrl
 * @description
 * # StylisCtrl
 * Controller of the projectxApp
 */
angular.module('projectxApp')
  .controller('StylisCtrl', ['$scope', '$uibModal', function ($scope, $uibModal) {

    $scope.user = { avatar: 'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/08/13/d7160f94-e74_1660129_100x100.jpg', name: 'Marco Nobre', place: 'Makeup Artist', business: 'Makeup By Ashley', studio: 'My Home Studio', location: 'Columbia, SC 29229',longitude: 51.5287336, latitude: -0.382471 }
    $scope.locObj = { latitude: $scope.user.longitude, longitude: $scope.user.latitude };
    $scope.map = { center: $scope.locObj, zoom: 15 };
    $scope.options = {
      scrollwheel: false,
      streetViewControl: false,
      zoomControl: false,
      streetViewControl: false,
      mapTypeControl : false
    };

    $scope.marker = {
      id: 0,
      coords: {
        latitude: $scope.locObj.latitude,
        longitude: $scope.locObj.longitude
      },
      options: { draggable: true }
    };

    // OpenImage Modal

    $scope.openImage = function(index) {
      $uibModal.open({
        templateUrl : 'views/modals/openImage.html',
        controller: 'ModalImageCtrl',
        resolve : {
          user: function() {
            return $scope.user;
          },
          index: function() {
            return index;
          },
          images : function() {
            return $scope.images;
          }
        }
      });
    };

    // Message Me Modal

    $scope.messageMe = function() {
      $uibModal.open({
        templateUrl : 'views/modals/messageMe.html',
        controller: 'ModalMessageMeCtrl'
      });
    };

    // requestApointment modal
    $scope.requestApointment = function() {
      $uibModal.open({
        templateUrl : 'views/modals/requestApointment.html',
        controller: 'ModalApointmentCtrl'
      });
    }

    $scope.writeRecommend = function() {
      $uibModal.open({
        templateUrl : 'views/modals/writeRecommend.html',
        controller: 'ModalWriteRecommendCtrl',
        resolve : {
          user: function() {
            return $scope.user;
          }
        }
      });
    }

    // MOCK DATA

    $scope.images = [
      'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/01/28/0c4d3687940a_1183116_720x540.jpg',
      'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/01/28/f8f9500eea31_1183123_720x540.jpg',
      'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/01/28/3a75d18709f7_1183124_720x540.jpg',
      'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/01/28/0c4d3687940a_1183116_720x540.jpg',
      'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/01/28/f8f9500eea31_1183123_720x540.jpg',
      'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/01/28/3a75d18709f7_1183124_720x540.jpg'
    ];

    $scope.notices = [
      {info: 'PLEASE READ ALL DISCRIPTIONS BEFORE BOOKING...THANK YOU.', detail: 'Please read the description of the services carefully to ensure that you are given proper time. Some services are grouped ...', price: '$0', time: '15 min.'},
      {info: 'PLEASE READ ALL DISCRIPTIONS BEFORE BOOKING...THANK YOU.', detail: 'Please read the description of the services carefully to ensure that you are given proper time. Some services are grouped ...', price: '$0', time: '15 min.'},
      {info: 'PLEASE READ ALL DISCRIPTIONS BEFORE BOOKING...THANK YOU.', detail: 'Please read the description of the services carefully to ensure that you are given proper time. Some services are grouped ...', price: '$0', time: '15 min.'}
    ];

    $scope.books = [
      {info: 'Bleach & Tone w/ Haircut', detail: 'This is to create an all over blonde look. Think Marylyn Monroe getting a haircut ;-)', price: '$225 and up', time: '2 hr., 30 min.'},
      {info: 'Bleach & Tone w/ Haircut', detail: 'This is to create an all over blonde look. Think Marylyn Monroe getting a haircut ;-)', price: '$225 and up', time: '2 hr., 30 min.'},
      {info: 'Bleach & Tone w/ Haircut', detail: 'This is to create an all over blonde look. Think Marylyn Monroe getting a haircut ;-)', price: '$225 and up', time: '2 hr., 30 min.'}
    ];

    $scope.colorServices = [
      {info: 'Bleach & Tone', detail: 'This is to create an all over blonde look. Think Marylyn Monroe!!! Service cover roots only with toner. If you ... ', price: '$130 and up', time: '2 hr., 50 min.'},
      {info: 'Bleach & Tone', detail: 'This is to create an all over blonde look. Think Marylyn Monroe!!! Service cover roots only with toner. If you ... ', price: '$130 and up', time: '2 hr., 50 min.'},
      {info: 'Bleach & Tone', detail: 'This is to create an all over blonde look. Think Marylyn Monroe!!! Service cover roots only with toner. If you ... ', price: '$130 and up', time: '2 hr., 50 min.'}
    ];

    $scope.recommends = [
      {name: 'Dee C.', date: '9/15/15', content: 'I love Gilbert Pickett! Definitely book your next appointment here!'},
      {name: 'Dee C.', date: '9/15/15', content: 'Gilbert was great! Takes time with you and listens to and addresses concerns about your hair. Has great recommendations. Very professional, positive, and honest.'},
      {name: 'Dee C.', date: '9/15/15', content: 'Gilbert was great! Takes time with you and listens to and addresses concerns about your hair. Has great recommendations. Very professional, positive, and honest.'},
      {name: 'Dee C.', date: '9/15/15', content: 'He really is amazing with his talent and attention to detail, and getting things right for the client. He gave me an incredible haircut and knew how to make the most of my hair, and the style.'}
    ];
  }]);
