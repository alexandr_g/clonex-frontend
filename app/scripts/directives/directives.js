'use strict';

var projectApp = angular.module('projectxApp');

projectApp.directive('scrollMap', function() {
	return {
		restrict : 'A',
		link : function(scope, elm, attr) {
			setTimeout(function() {
				var m = $('#map');
				var bottom = $('#results').offset().top + $('#results').height();
				var top = m.offset().top;
				var y;

				$(window).scroll(function() {
					y = $(window).scrollTop();

					if (y + 340 >= bottom) {

					} else if (y >= top) {
						m.addClass('fixed').css({ "margin-top": y - top + 10});
					} else{
	          // otherwise remove it
	          m.removeClass('fixed').css({ "margin-top": 0 });
	        }
				});
			})
		}
	}
});

projectApp.directive('playImage', function($rootScope) {
	return {
		restrict : 'A',
		link : function(scope, elm, attr) {
			scope.$on('changeImage', function(event, data) {
				// $('#displayImage').fadeOut(200, function() {
		    $('#displayImage').attr("src",data);
		    //     $('#displayImage').fadeIn(200);
		    // });
			});

			scope.$on('$destroy', function() {
				$('#projectxApp').off('keydown');
			});

			$('#projectxApp').keydown(function(e){
				switch(e.keyCode) {
					case 37:
						// left nav
						$rootScope.$broadcast('left');
						break;
					case 39:
						// right nav
						$rootScope.$broadcast('right');
						break;
				}
			});
		}
	}
});